const rpi_gpio = require('rpi-gpio'); //Ahora mismo no la estoy utilizando
const Gpio = require('onoff').Gpio;
const dht = require('node-dht-sensor');

/*Componentes y sus pines Gpio*/

const gpioSensor=4;
const modeloSensor=11; //DHT11

const titilarLeds = (pinGpio) => {
    let ledAlarmaInc= new Gpio(pinGpio, 'out');
    let interval = setInterval(function() {
        let value = (ledAlarmaInc.readSync() + 1) % 2;
        ledAlarmaInc.write(value, function() {
            console.log("Changed LED state to: " + value);
        });
    }, 2000);
    let useLed = (ledAlarmaInc, value) => ledAlarmaInc.writeSync(value);
    return interval;
};


const luzArtificial = (valor,pinGpio) => {
    /**
     * valor=1 prende led
     * valor=0 apaga led
     */
    let led = new Gpio(pinGpio, 'out');
    if (valor!=1 && valor !=0){
        console.log("Parametro incorrecto");
        return;
    } 
    led.write(valor, ()=> {
        if (valor==1) {
            console.log('Prendiendo luz artificial');
        } else {
            console.log('Apagando luz artificial');
        }
    } );
    let useLed = (led, value) => led.writeSync(value);
};
 
const leerHumedad=(callback) =>{
    dht.read(modeloSensor, gpioSensor, function(err, temperature, humidity) {
        if (!err) {
            // console.log('temp: ' + temperature.toFixed(1) + '°C, ' +
            //     'humidity: ' + humidity.toFixed(1) + '%'
            //     );
            let temperatura= temperature.toFixed(1);
            let humedad= humidity.toFixed(1);
            callback(temperatura,humedad);
        }else{
            console.log(err);
            
        }
    });
}
 

module.exports = {
    titilarLeds,
    luzArtificial,
    leerHumedad
}