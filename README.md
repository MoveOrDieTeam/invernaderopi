﻿Si van a probar la lectura de pines usando la raspberry descomenten la linea 5 del archivo index.js

Direcciones:
    Pagina principal:       127.0.0.1:8080/  
    Chat con web sockets:   127.0.0.1:8080/salchichon
    Sistema cheto:          127.0.0.1:8080/dashboard
    Consola secreta:        127.0.0.1:8080/secret

Diseño web:
https://github.com/designmodo/Flat-UI
https://designmodo.github.io/Flat-UI/docs/components.html
http://designmodo.github.io/Flat-UI/
https://www.chartjs.org/

Lector de sensores:
https://www.jmramirez.pro/tutorial/raspberry-pi-qnap-qiot-suite-lite/

Librerias:
https://www.npmjs.com/package/rpi-gpio //Actualmente no implementada
https://www.npmjs.com/package/onoff
https://www.npmjs.com/package/node-dht-sensor	//Hablar con profesor