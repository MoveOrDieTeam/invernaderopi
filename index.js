const fs = require('fs'); //require filesystem module
const http = require('http');
const express = require('express');
const path = require('path');
const wSockets = require('./webSockets');
const Gpio = require('./leerPines'); //Solo des-comentar cuando se conecte a las raspberry

const app = express();
var server = app.listen(8810);
const hostname = '127.0.0.1';
const port = 8080;

//app.set('view engine', 'pug');

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/views/index.html');
});

app.get('/salchichon', (req, res) => {
    res.sendFile(__dirname + '/views/salchichon.html');
});

app.use(express.static(path.join(__dirname, "views")))

app.get('/dashboard', (req, res) => {
    res.sendFile(__dirname + '/views/index.html');
});

app.get('/secret', (req, res) => {
    res.sendFile(__dirname + '/views/consolaSecreta.html');
});

app.listen(port, () => {
    console.log(`Servidor corriendo en http://${hostname}:${port}/`);
});

wSockets.inicializarSockets(server);