const socket = require('socket.io');
const pines= require('./leerPines');
const Sound = require('node-aplay');
 
// with ability to pause/resume:
var musicIncendio = new Sound('sounds/incendio.wav');
var musicSequia = new Sound('sounds/gta.wav');


const inicializarSockets = (server) => {
    let io= socket.listen(server);
    const connections = [];
    io.sockets.on('connection', (socket) => {
        
        var temp;
        var humedad;
        var luz = 110;
        var incendio=0;
        var sequia=0;
        var riego=0;

        setInterval(() => {
            pines.leerHumedad( (temperatura,humidity)=>{
                temp=temperatura;
                humedad=humidity;
                io.sockets.emit('humedad', { status: humedad });  
                io.sockets.emit('temperatura', { status: temp });
                controlGeneral();
        });
        }, 20000);

        io.sockets.emit('temperatura', { status: temp });
        io.sockets.emit('luz', { status: luz });
        //io.sockets.emit('humedad', { status: humedad });
        connections.push(socket);
        console.log(' %s sockets is connected', connections.length);

        socket.on('disconnect', () => {
            connections.splice(connections.indexOf(socket), 1);
        });

        socket.on('mensaje', (message) => {
            console.log('Message is mensaje :', message);

            io.sockets.emit('mensaje', { message: message });
        });

        socket.on('led', (message) => {
            console.log('Message is led :', message);

            io.sockets.emit('led', { status: message });
        });

        socket.on('alarmaIncendio', (message) => {
            console.log('Message is alarma :', message);

            io.sockets.emit('alarmaIncendio', { status: message });
        });

        socket.on('alarmaSequia', (message) => {
            console.log('Message is alarma :', message);

            io.sockets.emit('alarmaSequia', { status: message });
        });

        socket.on('motor', (message) => {
            console.log('Message is motor :', message);

            io.sockets.emit('motor', { status: message });
        });

        socket.on('temperatura', (message) => {
            console.log('Message is temperatura :', message);
            temp = message;
            controlGeneral();
            io.sockets.emit('temperatura', { status: message });
        });

        socket.on('luz', (message) => {
            console.log('Message is luz :', message);
            luz = message;
            controlGeneral();
            io.sockets.emit('luz', { status: message });
        });

        socket.on('humedad', (message) => {
            console.log('Message is humedad :', message);
            humedad = message;
            controlGeneral();
            io.sockets.emit('humedad', { status: message });
        });

        controlGeneral();

        function controlGeneral() {
            console.log("Control general");
            console.log(incendio);
            if(temp>50){
                console.log("Entro en el if");
                if(!incendio) incendio = prenderAlarmaIncendio();
                if(!riego) riego = prenderBombaRiego(100);

            }else if( humedad < 20){
                if(!riego) riego = prenderBombaRiego(70);
                if(!sequia) sequia = prenderAlarmaSequia();
            }else if( temp > 25 && humedad < 50){
                if(!riego) riego = prenderBombaRiego(25);
            }
            else{
                console.log("Hay que apagar la alarma");
                if(incendio) incendio = apagarAlarmaIncendio(incendio);
                if(sequia) sequia = apagarAlarmaSequia(sequia);
                if(riego) riego = apagarBombaRiego(riego);
            }

            if (luz<100) {
                io.sockets.emit('led', { status: true });
                pines.luzArtificial(1,17)
            }else{
                io.sockets.emit('led', { status: false });
                pines.luzArtificial(0,17);
            }
        }


        function prenderAlarmaIncendio(){
            io.sockets.emit('alarmaIncendio', { status: true });
            return alarmaIncendio= setInterval( ()=>{
                console.log("HOLAAAA");
                musicIncendio.play();
                
            },8000)
        }
        function prenderAlarmaSequia(){
            io.sockets.emit('alarmaSequia', { status: true });
            return alarmaIncendio= setInterval( ()=>{
                console.log("Prendi alarma sequia jeje");
                musicSequia.play();
            },8000)
        }
        
        function apagarAlarmaIncendio(incendio){
            io.sockets.emit('alarmaIncendio', { status: false });
            console.log("CXhau :( alamar incendio xDD");
            clearInterval(incendio);
            return 0;
            
        }
        function apagarAlarmaSequia(sequia){
            io.sockets.emit('alarmaSequia', { status: false });
            console.log("CXhau :( alarma sequia XD");
            clearInterval(sequia);
            return 0;
            
        }
        function prenderBombaRiego(caudal){
            io.sockets.emit('motor', { status: caudal });
            return pines.titilarLeds(16);
        }
        function apagarBombaRiego(riego){
            io.sockets.emit('motor', { status: 0 });
            clearInterval(riego);
            pines.luzArtificial(0,16);
        }
        
    });
}

const sockets= ()=>{
}

module.exports = {
    inicializarSockets
}